package cz.moneta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class Application {
    @GetMapping("/convert/{number:\\d+}")
    public String convert(@PathVariable String number) {
        return NumberConverter.convert(number);
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
