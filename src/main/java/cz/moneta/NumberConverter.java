package cz.moneta;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class NumberConverter {
    private NumberConverter() {
        throw new UnsupportedOperationException();
    }

    /**
     * Výpočet provede postupně takto:
     * <p>
     * - všechny číslice menší 3 (včetně) posune o jednu pozici doprava. Např: 43256791 => 45326791
     * <p>
     * - všechny číslice 8 a 9 vynásobí 2. Např.: 45326791 => 453267181
     * <p>
     * - všechny číslice 7 smaže: Např: 453267181 => 45326181
     * <p>
     * - ve výsledném čísle spočte počet sudých číslic a tímto počtem výsledné číslo vydělí a zaokrouhlí dolů na celá čísla. Např: 45326181 / 4 => 11331545
     */
    static String convert(String number) {
        var digits = getDigits(number);
        shiftLowDigitsRight(digits);
        multiplyHighDigitsByTwo(digits);
        purgeSevens(digits);
        var evenDigitCount = getEvenDigitCount(digits);
        return evenDigitCount > 0
                ? new BigInteger(toString(digits)).divide(BigInteger.valueOf(evenDigitCount)).toString()
                : toString(digits);
    }

    private static List<Integer> getDigits(String number) {
        var chars = number.toCharArray();
        var digits = new ArrayList<Integer>(chars.length);
        for (var c : chars) {
            digits.add(c - '0');
        }
        return digits;
    }

    private static void shiftLowDigitsRight(List<Integer> digits) {
        for (int i = digits.size() - 2; i >= 0; i--) {
            if (digits.get(i) <= 3 && digits.get(i + 1) > 3) {
                Collections.swap(digits, i, i + 1);
            }
        }
    }

    private static void multiplyHighDigitsByTwo(List<Integer> digits) {
        var it = digits.listIterator();
        while (it.hasNext()) {
            var digit = it.next();
            if (digit == 8 || digit == 9) {
                it.set(digit * 2);
            }
        }
    }

    private static void purgeSevens(List<Integer> digits) {
        digits.removeIf(digit -> digit == 7);
    }

    private static long getEvenDigitCount(List<Integer> digits) {
        return digits.stream()
                .filter(digit -> digit % 2 == 0)
                .count();
    }

    private static String toString(List<Integer> digits) {
        var sb = new StringBuilder();
        for (var digit : digits) {
            sb.append(digit);
        }
        return sb.toString();
    }
}
