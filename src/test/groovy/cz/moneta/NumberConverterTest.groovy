package cz.moneta

import spock.lang.Specification
import spock.lang.Unroll

class NumberConverterTest extends Specification {
    @Unroll
    def '\'#input\' is converted to \'#expected\''() {
        expect:
        NumberConverter.convert(input) == expected

        where:
        input      | expected
        '43256791' | '11331545'
        '005'      | '250'
        '111'      | '111'
        '3'        | '3'
        '5'        | '5'
        '7'        | ''
        '000'      | '0'
    }
}
